package io.pillopl.springboot.boot;

import io.pillopl.springboot.boot.Borrower;
import io.pillopl.springboot.boot.BorrowerRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
public class BorrowerMongoRepoTest {


	/**
	 * TASK: Zrob tak, zeby Borrower byl zapisany w Mongo.
	 * Spraw by testy przeszly (po odkomentowaniu linii 34)
	 * Podpowiedzi:
	 * sprawdz zaleznosci pom.xml
	 * @Entity juz sie nie przyda
	 * BorrowerRepository juz nie rozszerza CrudRepos
	 */
	@Autowired
	BorrowerRepository borrowerRepository;


	@Test
	public void shouldSaveNewBorrowerAnna() {
		//given
		Borrower anna = new Borrower("anna");
		//and
		borrowerRepository.save(anna);

		//expect
		Assertions.assertThat(borrowerRepository.findByName("anna").size()).isEqualTo(1);

	}


}
