package io.pillopl.springboot.boot;

import io.pillopl.springboot.boot.BorrowerRegistrationService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Sprawdź, aby do testu Spring Boot wstrzyknął klasę BorrowerRegistrationService
 *
 * Podpowiedzi:
 * 1. W Spring Boocie test integracyjny z klasą integracyjną można zestawić za pomocą adnotacji @SpringBootTest nad testem - zastanów się skąd wtedy będą wyciągane definicje beanów
 * 2. Należy zarejestrować klasę BorrowerRegistrationService
 * 3. Należy ją wstrzyknąć do testu.
 */
@SpringBootTest
public class Zadanie3_NotNullBeanTest {

	@Autowired
	BorrowerRegistrationService borrowerRegistrationService;

	@Test
	void contextLoads() {
		Assertions.assertThat(borrowerRegistrationService).isNotNull();
	}

}
