package io.pillopl.springboot.boot;

import io.pillopl.springboot.boot.BorrowerRepository;
import io.pillopl.springboot.boot.Channels;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class Zadanie13_StreamTest {

	@Autowired
	Channels channels;

	@Autowired
	BorrowerRepository borrowerRepository;

	//@Autowired
	//MessageCollector messageCollector;

	/**
	 * TASK: Listening from a queue and writing to queue
	 * 1. Zaimplementuj testy i kod produkcyjny
	 *  Jesli chcesz wysylac na channel; wyjsciowy, wstrzyknij definicje kanalow i zawolach channels.borrowers().send(...)
	 *  Jesli w tescie chcesz sprawdzic, ze cos sie wyslalo na kanal wyjsciowy - uzyj message collectora i zbieraj wiadomosci do BlockingQueue
	 *  BlockingQueue messages = messageCollector.forChannel(channels.risk());
	 *  Jesli chcesz sluchac z kanalu, dodaj w swoim beanie adnotacje @StreamListener(channel = ...) w jakis beanie.
	 *  Użyj interfejsu EventPublisher - zaimplementuj go.
	 */


	@BeforeEach
	public void setup() {

	}

	@Test
	public void listeningToChannelRiskResultsInNewBorrower() {
		//given
		//send info to input channel risk - "john"

		//expect
		//john is created

	}


	@Test
	public void changingBorrowerNameShouldResultWithOutputMessageSentToBorrowersChannel() {
		//given
		//create borrower ANNA
		//change his name to "JOHN"

		//expect
		//a message "ANNA is now JOHN" on output channel - borrowers

	}


}
