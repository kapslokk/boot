package io.pillopl.springboot.boot.card;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.stream.IntStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CardHolderControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    CardHolderRepository cardHolderRepository;

    @Test
    void canWithdraw() throws Exception {
        //given
        CardHolder cardHolder = aCardHolderWithLimit(new BigDecimal(100));

        //expect
        mockMvc
                .perform(post("/holders/" + cardHolder.getBorrowerId())
                        .header("Content-Type", "application/json")
                        .content("{\"amount\": \"101\"}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void cannotWithdrawBecauseLimitOfWithdrawalsExceeded() throws Exception {
        //given
        CardHolder cardHolder = aCardHolderWithLimit(new BigDecimal(100));

        //and
        manyWithdrawalsOfValueOneWereDone(45, cardHolder);

        //expect
        mockMvc
                .perform(post("/holders/" + cardHolder.getBorrowerId())
                        .header("Content-Type", "application/json")
                        .content("{\"amount\": \"1\"}"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void cannotWithdrawBecauseLimitExceeded() throws Exception {
        //given
        CardHolder cardHolder = aCardHolderWithLimit(new BigDecimal(100));

        //expect
        mockMvc
                .perform(post("/holders/" + cardHolder.getBorrowerId())
                        .header("Content-Type", "application/json")
                        .content("{\"amount\": \"111\"}"))
                .andExpect(status().is4xxClientError());
    }

    private void manyWithdrawalsOfValueOneWereDone(int withdrawalsCount, CardHolder cardHolder) {
        IntStream.range(1, withdrawalsCount + 1).forEach(i -> {
            try {
                mockMvc
                        .perform(post("/holders/" + cardHolder.getBorrowerId())
                                .header("Content-Type", "application/json")
                                .content("{\"amount\": \"1\"}"))
                                .andExpect(status().is2xxSuccessful());

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private CardHolder aCardHolderWithLimit(BigDecimal limit) {
        return cardHolderRepository.save(new CardHolder(10, limit));
    }

    @Test
    void cannotWithdrawBecauseWithdrawalsExceeded() {

    }

}