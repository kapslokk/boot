package io.pillopl.springboot.boot.card;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CommentsControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    CardHolderRepository cardHolderRepository;

    @Test
    void commentsShouldBeVisible() throws Exception {
        //given
        CardHolder cardHolder = aCardHolderWithLimit(new BigDecimal(100));
        CardHolder cardHolder2 = aCardHolderWithLimit(new BigDecimal(100));

        //and
        withdrawalBy(cardHolder);
        withdrawalBy(cardHolder2);

        //then
        mockMvc
                .perform(get("/comments/"))
                .andExpect(content().json("{\"comments\":[\"withdrawal\",\"withdrawal\"]}"))
                .andExpect(status().is2xxSuccessful());
    }

    private void withdrawalBy(CardHolder cardHolder) throws Exception {
        mockMvc
                .perform(post("/holders/" + cardHolder.getBorrowerId())
                        .header("Content-Type", "application/json")
                        .content("{\"amount\": \"1\"}"))
                .andExpect(status().is2xxSuccessful());
    }

    private CardHolder aCardHolderWithLimit(BigDecimal limit) {
        return cardHolderRepository.save(new CardHolder(10, limit));
    }


}