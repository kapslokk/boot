package io.pillopl.springboot.di.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class TestPaymentConfiguration {

    @Bean
    PaymentGateway paymentGateway(CanAuthenticate authenticationManager) {
        return new PaymentGateway(authenticationManager, new BlueCashService(), new PaymentDetailsProvider(new PaymentTitleProvider(), new ClientDetailsProvider()));
    }

    @Bean
    CanAuthenticate authenticationManager() {
        return new CanAuthenticate() {
            @Override
            public String authenticate(String user) {
                return "NOT_OK";
            }
        };
    }



}
