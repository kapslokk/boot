package io.pillopl.springboot.boot;

public interface EventPublisher {

    //powinnismy użyć klasy w stylu DomainEvent jako parametru
    void publish(String event);
}
