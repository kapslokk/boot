package io.pillopl.springboot.boot.offer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
class OfferController {

    private final OfferRepository offerRepository;

    OfferController(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    @GetMapping("/offers")
    ResponseEntity offers() {

        Collection<Offer> all = offerRepository.all();
        if(all.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(all);
    }
}


