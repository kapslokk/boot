package io.pillopl.springboot.boot.card;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
public class CommentsController {

    private final CardHolderRepository cardHolderRepository;

    public CommentsController(CardHolderRepository cardHolderRepository) {
        this.cardHolderRepository = cardHolderRepository;
    }

    @GetMapping("/comments")
    ResponseEntity comments() {
        List<String> comments = cardHolderRepository.comments();
        return ok(new CommentsView(comments));
    }



}


class CommentsView {
    private List<String> comments;

    CommentsView(List<String> comments) {
        this.comments = comments;
    }


    public List<String> getComments() {
        return comments;
    }
}