package io.pillopl.springboot.boot.card;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

import static org.springframework.http.ResponseEntity.ok;

@RestController
public class CardHolderController {

    private final WithdrawService withdrawService;

    public CardHolderController(WithdrawService withdrawService) {
        this.withdrawService = withdrawService;
    }

    @PostMapping("/holders/{holderId}")
    ResponseEntity withdraw(@RequestBody WithdrawalRequest wr, @PathVariable Long holderId) {
        withdrawService.withdraw(holderId, wr.getAmount());
        return ok().build();
    }
}

class WithdrawalRequest {
    private BigDecimal amount;

    WithdrawalRequest(BigDecimal amount) {
        this.amount = amount;
    }

    WithdrawalRequest() {

    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}