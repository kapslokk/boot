package io.pillopl.springboot.boot.card;

import javax.annotation.processing.Generated;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
class CardHolder {

    public CardHolder() {
    }

    @Id
    @GeneratedValue
    private Long borrowerId;
    private int age;
    private BigDecimal availableLimit;
    private BigDecimal extraLimit;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Withdrawal> withdrawals = new ArrayList<>();

    CardHolder(int age, BigDecimal availableLimit) {
        this.age = age;
        this.availableLimit = availableLimit;
    }

    void withdraw(BigDecimal withdrawalAmount) {
        if (withdrawalAmount.compareTo(availableLimit) > 0) {
            throw new CannotWithdrawException("Too little money");
        }
        if (withdrawals.size() >= 45) {
            throw new CannotWithdrawException("Too little money");
        }
        Withdrawal withdrawal = new Withdrawal(withdrawalAmount, Instant.now());
        withdrawal.addComment("withdrawal");
        withdrawals.add(withdrawal);
        availableLimit = availableLimit.subtract(withdrawalAmount);
    }

    BigDecimal availableLimit() {
        return availableLimit;
    }

    boolean isAtLeast20yo() {
        return age >= 0;
    }

    public Long getBorrowerId() {
        return borrowerId;
    }

    public boolean has(BigDecimal amountToPay) {
        return availableLimit.compareTo(amountToPay) >= 0;
    }

    public void pay(BigDecimal amountToPay) {
        availableLimit = availableLimit.subtract(amountToPay);
    }

    public void payUsingExtraLimit(BigDecimal amountToPay) {
        extraLimit = extraLimit.subtract(amountToPay);
    }

    List<Withdrawal> getWithdrawals() {
        return withdrawals;
    }
}


