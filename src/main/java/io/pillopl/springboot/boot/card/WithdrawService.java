package io.pillopl.springboot.boot.card;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@Service
class WithdrawService {

    private final CardHolderRepository cardHolderRepository;

    WithdrawService(CardHolderRepository cardHolderRepository) {
        this.cardHolderRepository = cardHolderRepository;
    }

    @Transactional
    void withdraw(Long cardHolderId, BigDecimal withdrawalAmount) {
        CardHolder cardHolder = cardHolderRepository.findById(cardHolderId)
                .orElseThrow(IllegalArgumentException::new);
        cardHolder.withdraw(withdrawalAmount);
        cardHolderRepository.save(cardHolder);
    }
}
