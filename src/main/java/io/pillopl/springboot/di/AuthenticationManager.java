package io.pillopl.springboot.di;

import java.util.Random;

public class AuthenticationManager {
    String authenticate(String userDetails) {
        if (new Random().nextBoolean()) {
            return "ERROR";
        }
        boolean isHonest = userDetails.equals("HONEST_USER");
        if (isHonest) {
            return "OK";
        }
        return "NOT_OK";
    }
}
