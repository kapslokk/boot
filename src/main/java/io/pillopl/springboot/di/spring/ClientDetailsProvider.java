package io.pillopl.springboot.di.spring;

import java.util.Random;

public class ClientDetailsProvider {

    String loadClientDetails(String userName) {
        //zawodne wołanie innego serwisu - np serwis klasy 3rd party
        if (new Random().nextBoolean()) {
            throw new IllegalStateException();
        }
        return "client details";
    }
}
