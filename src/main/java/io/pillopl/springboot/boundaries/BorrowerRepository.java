package io.pillopl.springboot.boundaries;

interface BorrowerRepository {

    Borrower findById(BorrowerId borrowerId);
}
