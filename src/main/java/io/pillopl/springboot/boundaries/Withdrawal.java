package io.pillopl.springboot.boundaries;

import java.math.BigDecimal;
import java.time.Instant;

class Withdrawal {
    private final BigDecimal amount;
    private final Instant timestamp;

    Withdrawal(BigDecimal amount, Instant timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    boolean isMoreThan(BigDecimal amount) {
        return this.amount.compareTo(amount) > 0;
    }
}
